# NASA Earthdata Drupal 9 Docker Stack

## Introduction

A set of docker images optimized for Drupal. Use the `docker-compose.yml` to spin up local environment on Linux, Mac OS X and Windows.

## Stack

The Drupal stack consist of the following containers:

| Container       | Versions                | Image                              | ARM64 support | Enabled by default |
|-----------------|-------------------------|------------------------------------|---------------|--------------------|
| [Apache]        | 2.4                     | [wodby/apache]                     | ✓             | ✓                  |
| [PHP]           | 8.1, 8.0, 7.4           | [wodby/drupal-php]                 | ✓             | ✓                  |
| Crond           |                         | [wodby/drupal-php]                 | ✓             | ✓                  |
| [MariaDB]       | 10.6, 10.5, 10.4, 10.3  | [wodby/mariadb]                    | ✓             | ✓                  |
| [Redis]         | 6, 5                    | [wodby/redis]                      | ✓             | ✓                  |
| Adminer         | 4.6                     | [wodby/adminer]                    |               | ✓                  |
| Traefik         | latest                  | [_/traefik]                        | ✓             | ✓                  |
 
Supported Drupal versions: 9 / 8 / 7

## License

This project is licensed under the MIT open source license.

[Apache]: https://wodby.com/docs/stacks/drupal/containers#apache
[MariaDB]: https://wodby.com/docs/stacks/drupal/containers#mariadb
[PHP]: https://wodby.com/docs/stacks/drupal/containers#php
[Redis]: https://wodby.com/docs/stacks/drupal/containers#redis

[_/traefik]: https://hub.docker.com/_/traefik
[wodby/adminer]: https://hub.docker.com/r/wodby/adminer
[wodby/apache]: https://github.com/wodby/apache
[wodby/drupal-php]: https://github.com/wodby/drupal-php
[wodby/mariadb]: https://github.com/wodby/mariadb
[wodby/redis]: https://github.com/wodby/redis
